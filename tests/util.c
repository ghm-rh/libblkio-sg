#include <signal.h>
#include "util.h"

static void (*cleanup)(void);

static void signal_handler(int signo)
{
	cleanup();
}

/* Call fn() at exit or when the process aborts */
void register_cleanup(void (*fn)(void))
{
	struct sigaction sigact = {
		.sa_handler = signal_handler,
		.sa_flags = SA_RESETHAND,
	};

	cleanup = fn;

	sigemptyset(&sigact.sa_mask);
	sigaction(SIGABRT, &sigact, NULL);

	atexit(fn);
}

/* Like mkstemp(3) except it also sets the file size */
int create_file(char *namebuf, off_t length)
{
	int fd = mkstemp(namebuf);
	assert(fd >= 0);

	assert(ftruncate(fd, length) == 0);

	return fd;
}
