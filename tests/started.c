// SPDX-License-Identifier: LGPL-2.1-or-later
#include "util.h"

enum {
	TEST_FILE_SIZE = 2 * 1024 * 1024,
};

static char filename[] = "started-XXXXXX";

static void cleanup(void)
{
	unlink(filename);
}

int main(void)
{
	struct blkio *b;
	char *errmsg;
	int fd;

	ok(blkio_create("io_uring", &b, &errmsg));
	assert(b);

	register_cleanup(cleanup);
	fd = create_file(filename, TEST_FILE_SIZE);

	ok(blkio_set_int(b, "fd", fd, &errmsg));
	ok(blkio_set_bool(b, "initialized", true, &errmsg));

	fd = -1; /* ownership passed to libblkio */

	ok(blkio_set_bool(b, "started", true, &errmsg));

	/* Must set started to false before setting intialized to false */
	err(blkio_set_bool(b, "initialized", false, &errmsg), -EBUSY);

	ok(blkio_set_bool(b, "started", false, &errmsg));
	ok(blkio_set_bool(b, "initialized", false, &errmsg));

	/* Need fd or path (previous fd was consumed) */
	err(blkio_set_bool(b, "initialized", true, &errmsg), -EINVAL);

	blkio_destroy(&b);
	assert(!b);

	return 0;
}
