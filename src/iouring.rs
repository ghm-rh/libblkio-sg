// TODO Implement BLKIO_REQ_POLLED by setting IORING_CQ_EVENTFD_DISABLED to suppress eventfd
// completion events. The io_uring crate doesn't support IORING_CQ_EVENTFD_DISABLED yet.

use crate::blkio::{Blkioq, Completion, Driver, Error, Queue, Result, BLKIO_REQ_FUA};
use crate::properties;
use crate::properties::{PropertiesList, Property};
use io_uring::opcode::{Fsync, Read, Readv, Write, Writev};
use io_uring::types::{Fd, FsyncFlags};
use libc::{
    close, eventfd, iovec, sigset_t, EBUSY, EFD_CLOEXEC, EFD_NONBLOCK, EINVAL, ENOBUFS, ENOMEM,
    ENOTSUP, O_DIRECT, RWF_DSYNC,
};
use std::fs::{File, OpenOptions};
use std::os::unix::fs::OpenOptionsExt;
use std::os::unix::io::{AsRawFd, FromRawFd, RawFd};
use std::time::Duration;

// The <linux/io_uring.h> constant is missing from the io_uring crate
const IORING_MAX_ENTRIES: i32 = 32768;

// Hardware queue depth 64-128 is common so use that as the default
const NUM_DESCS_DEFAULT: i32 = 128;

// The io_uring crate exposes a low-level io_uring_enter(2) interface via IoUring.enter() but the
// flag argument constants are private in io_uring::sys. Redefine the value from <linux/io_uring.h>
// here for now.
const IORING_ENTER_GETEVENTS: u32 = 1;

struct IoUringQueue {
    ring: io_uring::IoUring,
    fd: Fd,
    eventfd: RawFd,
}

impl IoUringQueue {
    pub fn new(num_descs: u32, fd: RawFd) -> Result<Self> {
        let ring =
            io_uring::IoUring::new(num_descs).map_err(|e| Error::from_io_error(e, -ENOMEM))?;

        // TODO register fd

        let eventfd = unsafe { eventfd(0, EFD_CLOEXEC | EFD_NONBLOCK) };
        if eventfd < 0 {
            return Err(Error::from_io_error(
                std::io::Error::last_os_error(),
                -ENOMEM,
            ));
        }
        ring.submitter()
            .register_eventfd(eventfd)
            .map_err(|e| Error::from_io_error(e, -ENOTSUP))?;

        Ok(IoUringQueue {
            ring: ring,
            fd: Fd(fd),
            eventfd: eventfd,
        })
    }
}

impl Drop for IoUringQueue {
    fn drop(&mut self) {
        unsafe { close(self.eventfd) };
        self.eventfd = -1;
    }
}

impl Queue for IoUringQueue {
    fn get_completion_fd(&self) -> Result<i32> {
        Ok(self.eventfd)
    }

    fn read(
        &mut self,
        start: u64,
        buf: *mut u8,
        len: usize,
        user_data: usize,
        _flags: u32,
    ) -> Result<()> {
        if len > u32::MAX as usize {
            return Err(Error(-EINVAL, "len must be 32-bit".to_string()));
        }
        let entry = Read::new(self.fd, buf, len as u32)
            .offset(start as i64)
            .build()
            .user_data(user_data as u64);
        let result = unsafe { self.ring.submission().push(&entry) };
        if result.is_ok() {
            Ok(())
        } else {
            Err(Error(-ENOBUFS, "submission queue is full".to_string()))
        }
    }

    fn write(
        &mut self,
        start: u64,
        buf: *const u8,
        len: usize,
        user_data: usize,
        flags: u32,
    ) -> Result<()> {
        if len > u32::MAX as usize {
            return Err(Error(-EINVAL, "len must be 32-bit".to_string()));
        }

        let rw_flags = if flags & BLKIO_REQ_FUA != 0 {
            RWF_DSYNC
        } else {
            0
        };

        let entry = Write::new(self.fd, buf, len as u32)
            .offset(start as i64)
            .rw_flags(rw_flags)
            .build()
            .user_data(user_data as u64);
        let result = unsafe { self.ring.submission().push(&entry) };
        if result.is_ok() {
            Ok(())
        } else {
            Err(Error(-ENOBUFS, "submission queue is full".to_string()))
        }
    }

    fn readv(
        &mut self,
        start: u64,
        iovec: *const iovec,
        iovcnt: u32,
        user_data: usize,
        _flags: u32,
    ) -> Result<()> {
        let entry = Readv::new(self.fd, iovec, iovcnt)
            .offset(start as i64)
            .build()
            .user_data(user_data as u64);
        let result = unsafe { self.ring.submission().push(&entry) };
        if result.is_ok() {
            Ok(())
        } else {
            Err(Error(-ENOBUFS, "submission queue is full".to_string()))
        }
    }

    fn writev(
        &mut self,
        start: u64,
        iovec: *const iovec,
        iovcnt: u32,
        user_data: usize,
        flags: u32,
    ) -> Result<()> {
        let rw_flags = if flags & BLKIO_REQ_FUA != 0 {
            RWF_DSYNC
        } else {
            0
        };

        let entry = Writev::new(self.fd, iovec, iovcnt)
            .offset(start as i64)
            .rw_flags(rw_flags)
            .build()
            .user_data(user_data as u64);

        let result = unsafe { self.ring.submission().push(&entry) };
        if result.is_ok() {
            Ok(())
        } else {
            Err(Error(-ENOBUFS, "submission queue is full".to_string()))
        }
    }

    fn flush(&mut self, user_data: usize, _flags: u32) -> Result<()> {
        let entry = Fsync::new(self.fd)
            .flags(FsyncFlags::DATASYNC)
            .build()
            .user_data(user_data as u64);
        let result = unsafe { self.ring.submission().push(&entry) };
        if result.is_ok() {
            Ok(())
        } else {
            Err(Error(-ENOBUFS, "submission queue is full".to_string()))
        }
    }

    fn submit_and_wait(
        &mut self,
        min_completions: u32,
        timeout: Option<Duration>,
        sig: Option<&sigset_t>,
    ) -> Result<()> {
        // TODO implement timeout with Linux 5.11 IORING_ENTER_EXT_ARG in the future
        if timeout.is_some() {
            return Err(Error(
                -ENOTSUP,
                "io_uring submit_and_wait() timeout not yet implemented".to_string(),
            ));
        }

        let to_submit = self.ring.submission().len() as u32;
        let flags = if min_completions > 0 {
            IORING_ENTER_GETEVENTS
        } else {
            0
        };

        match unsafe {
            self.ring
                .submitter()
                .enter(to_submit, min_completions, flags, sig)
        } {
            Err(r) => Err(Error::from_io_error(r, -EINVAL)),
            _ => Ok(()),
        }
    }

    fn completions(&mut self) -> Box<dyn Iterator<Item = Completion> + '_> {
        Box::new(self.ring.completion().map(|entry| Completion {
            user_data: entry.user_data() as usize,
            ret: entry.result(),
        }))
    }
}

properties! {
    IOURING_PROPS: PropertyState for IoUring.props {
        fn capacity: u64,
        mut direct: bool,
        driver: str,
        mut fd: i32,
        mut initialized: bool,
        max_descs: i32,
        max_queues: i32,
        mut num_descs: i32,
        mut num_queues: i32,
        mut path: str,
        mut readonly: bool,
        mut started: bool
    }
}

pub struct IoUring {
    props: PropertyState,
    file: Option<File>,
    queues: Vec<Blkioq>,
}

impl IoUring {
    pub fn new() -> Self {
        IoUring {
            props: PropertyState {
                direct: false,
                driver: "io_uring".to_string(),
                fd: -1,
                initialized: false,
                max_descs: IORING_MAX_ENTRIES,
                max_queues: i32::MAX,
                num_descs: NUM_DESCS_DEFAULT,
                num_queues: 1,
                path: String::new(),
                readonly: false,
                started: false,
            },
            file: None,
            queues: Vec::new(),
        }
    }

    fn cant_set_while_initialized(&self) -> Result<()> {
        if self.props.initialized {
            Err(properties::error_cant_set_while_initialized())
        } else {
            Ok(())
        }
    }

    fn cant_set_while_started(&self) -> Result<()> {
        if self.props.started {
            Err(properties::error_cant_set_while_started())
        } else {
            Ok(())
        }
    }

    fn must_be_initialized(&self) -> Result<()> {
        if self.props.initialized {
            Ok(())
        } else {
            Err(properties::error_must_be_initialized())
        }
    }

    fn must_be_started(&self) -> Result<()> {
        if self.props.started {
            Ok(())
        } else {
            Err(Error(-EBUSY, "Device must be started".to_string()))
        }
    }

    fn get_capacity(&self) -> Result<u64> {
        self.must_be_initialized()?;

        let len = self
            .file
            .as_ref()
            .map_or(0, |f| f.metadata().map_or(0, |m| m.len()));
        Ok(len)
    }

    fn set_direct(&mut self, value: bool) -> Result<()> {
        self.cant_set_while_initialized()?;
        self.props.direct = value;
        Ok(())
    }

    fn set_fd(&mut self, value: i32) -> Result<()> {
        self.cant_set_while_initialized()?;
        self.props.fd = value;
        Ok(())
    }

    // Open the file into self.fd
    fn open_file(&mut self) -> Result<()> {
        if self.props.path != "" {
            if self.props.fd != -1 {
                return Err(Error(
                    -EINVAL,
                    "path and fd cannot be set at the same time".to_string(),
                ));
            }

            let open_flags = if self.props.direct { O_DIRECT } else { 0 };

            let file = OpenOptions::new()
                .custom_flags(open_flags)
                .read(true)
                .write(!self.props.readonly)
                .open(self.props.path.as_str())
                .map_err(|e| Error::from_io_error(e, -EINVAL))?;

            self.props.fd = file.as_raw_fd();
            self.file = Some(file);
            Ok(())
        } else if self.props.fd != -1 {
            self.file = Some(unsafe { File::from_raw_fd(self.props.fd) });
            Ok(())
        } else {
            Err(Error(-EINVAL, "One of path and fd must be set".to_string()))
        }
    }

    fn close_file(&mut self) {
        if self.file.is_some() {
            self.props.fd = -1;
            self.file = None
        }
    }

    fn initialize(&mut self) -> Result<()> {
        self.open_file()?;
        Ok(())
    }

    fn uninitialize(&mut self) -> Result<()> {
        self.close_file();
        Ok(())
    }

    fn set_initialized(&mut self, value: bool) -> Result<()> {
        self.cant_set_while_started()?;

        if self.props.initialized == value {
            return Ok(());
        }

        if value {
            self.initialize()?
        } else {
            self.uninitialize()?
        }

        self.props.initialized = value;
        Ok(())
    }

    fn set_num_descs(&mut self, value: i32) -> Result<()> {
        self.must_be_initialized()?;
        self.cant_set_while_started()?;

        // TODO check power of two?
        if value <= 0 {
            return Err(Error(
                -EINVAL,
                "num_descs must be greater than 0".to_string(),
            ));
        }
        if value > IORING_MAX_ENTRIES {
            return Err(Error(
                -EINVAL,
                format!("num_descs must be smaller than {}", IORING_MAX_ENTRIES),
            ));
        }

        self.props.num_descs = value;
        Ok(())
    }

    fn set_num_queues(&mut self, value: i32) -> Result<()> {
        self.must_be_initialized()?;
        self.cant_set_while_started()?;

        if value <= 0 {
            return Err(Error(
                -EINVAL,
                "num_queues must be greater than 0".to_string(),
            ));
        }

        self.props.num_queues = value;
        Ok(())
    }

    fn set_path(&mut self, value: &str) -> Result<()> {
        self.cant_set_while_initialized()?;
        self.props.path = value.to_string();
        Ok(())
    }

    fn set_readonly(&mut self, value: bool) -> Result<()> {
        self.cant_set_while_initialized()?;
        self.props.readonly = value;
        Ok(())
    }

    fn start(&mut self) -> Result<()> {
        self.queues = Vec::with_capacity(self.props.num_queues as usize);

        for _ in 0..self.queues.capacity() {
            let raw_fd = self.file.as_ref().map(|f| f.as_raw_fd()).unwrap();
            match IoUringQueue::new(self.props.num_descs as u32, raw_fd) {
                Ok(q) => self.queues.push(Blkioq::new(Box::new(q))),
                Err(err) => {
                    self.queues = Vec::new();
                    return Err(err);
                }
            }
        }

        Ok(())
    }

    fn stop(&mut self) -> Result<()> {
        self.queues = Vec::new();
        Ok(())
    }

    fn set_started(&mut self, value: bool) -> Result<()> {
        self.must_be_initialized()?;

        if self.props.started == value {
            return Ok(());
        }

        if value {
            self.start()?
        } else {
            self.stop()?
        }

        self.props.started = value;
        Ok(())
    }
}

impl Driver for IoUring {
    // IORING_REGISTER_BUFFERS could be used in the future to improve performance. Ignore
    // memory regions for now.
    fn add_mem_region(
        &mut self,
        _addr: usize,
        _len: usize,
        _fd: RawFd,
        _fd_offset: i64,
    ) -> Result<()> {
        self.must_be_started()
    }

    fn del_mem_region(&mut self, _addr: usize, _len: usize) -> Result<()> {
        self.must_be_started()
    }

    fn get_queue(&mut self, index: usize) -> Result<&mut Blkioq> {
        self.queues
            .get_mut(index)
            .ok_or_else(|| Error(-EINVAL, "invalid queue index".to_string()))
    }
}
