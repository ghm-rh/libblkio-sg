// SPDX-License-Identifier: LGPL-2.1-or-later
#include <sys/mman.h>
#include "util.h"

enum {
    TEST_FILE_SIZE = 2 * 1024 * 1024,
    TEST_FILE_OFFSET = 0x10000,
};

static char filename[] = "writev-XXXXXX";

static void cleanup(void)
{
    unlink(filename);
}

/*
 * Write a well-known pattern at an offset in a file.
 */
int main(void)
{
    struct blkio *b;
    struct blkioq *q;
    struct blkio_mem_region mem_region;
    struct blkio_completion completion;
    void *buf;     /* I/O buffer */
    struct iovec iovecs[3];
    int iovcnt = sizeof(iovecs) / sizeof(iovecs[0]);
    void *pattern; /* reference data at TEST_FILE_OFFSET */
    size_t buf_size;
    size_t sub_buf_size;
    char *errmsg;
    int fd;
    int mfd;

    /* Set up I/O buffer */
    sub_buf_size = sysconf(_SC_PAGESIZE);
    buf_size = iovcnt * sysconf(_SC_PAGESIZE);
    mfd = memfd_create("buf", MFD_CLOEXEC);
    assert(mfd >= 0);
    assert(ftruncate(mfd, buf_size) == 0);
    buf = mmap(NULL, buf_size, PROT_READ | PROT_WRITE, MAP_SHARED, mfd, 0);
    assert(buf != MAP_FAILED);

    mem_region = (struct blkio_mem_region) {
        .addr = buf,
        .len = buf_size,
        .fd_offset = 0,
        .fd = mfd,
    };

    iovecs[0] = (struct iovec){
        .iov_base = buf,
        .iov_len = sub_buf_size,
    };
    iovecs[1] = (struct iovec){
        .iov_base = buf + sub_buf_size,
        .iov_len = sub_buf_size,
    };
    iovecs[2] = (struct iovec){
        .iov_base = buf + 2 * sub_buf_size,
        .iov_len = sub_buf_size,
    };

    /* Initialize pattern buffer */
    pattern = malloc(buf_size);
    assert(pattern);
    memset(pattern, 'A', sub_buf_size);
    memset(pattern + sub_buf_size, 'B', sub_buf_size);
    memset(pattern + 2 * sub_buf_size, 'C', sub_buf_size);

    memcpy(buf, pattern, buf_size);

    ok(blkio_create("io_uring", &b, &errmsg));
    assert(b);

    register_cleanup(cleanup);
    fd = create_file(filename, TEST_FILE_SIZE);

    ok(blkio_set_int(b, "fd", dup(fd), &errmsg));
    ok(blkio_set_bool(b, "initialized", true, &errmsg));
    ok(blkio_set_int(b, "num-queues", 1, &errmsg));
    ok(blkio_set_bool(b, "started", true, &errmsg));
    ok(blkio_add_mem_region(b, &mem_region, &errmsg));

    q = blkio_get_queue(b, 0);
    assert(q);

    assert(blkioq_writev(q, TEST_FILE_OFFSET, iovecs, iovcnt, NULL, 0) == 0);
    assert(blkioq_submit_and_wait(q, 1, NULL, NULL) == 0);
    assert(blkioq_get_completions(q, &completion, 1) == 1);
    assert(completion.ret == buf_size);

    memset(buf, 0, buf_size);
    assert(pread(fd, buf, buf_size, TEST_FILE_OFFSET) == buf_size);
    assert(memcmp(buf, pattern, buf_size) == 0);

    close(fd);
    blkio_destroy(&b);
    return 0;
}
